package estructuras;

import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ArrayList;


public class MapGraph implements Iterable<MapNode>
{
	/**
	 *  Diccionario que permite establecer una relacion entre
	 *  el id de un nodo y el nodo en si
	 **/
	private Map<Integer, MapNode> nodes;

	/**
	 * Diccionario que representa la lista de adyacencia principal del
	 * grafo que abstrae el mapa de vuelos.
	 **/
	private Map<Integer, Set<MapEdge>> edges;

	/**
	 * Arreglo con las llaves del grafo
	 */
	private List<Integer> keys;

	/**
	 * Constructor principal del grafo.
	 **/
	public MapGraph()
	{
		//TODO Inicialice nodes y edges utilizando un HashMap:
		
		this.keys = new ArrayList<Integer>();
	}

	/**
	 * Agrega el identificador y la informacion que describe a un nodo del
	 * mapa.
	 *
	 * @param id Numero de identificacion unica del nodo en el mapa de vuelos. id >= 0
	 * @param latitude Numero que establece la latitud geografica del nodo en el mapa real.
	 * @param longitude Numero que establece la longitud geografica del nodo en el mapa real.
	 */
	public void addNode(int id, double latitude, double longitude, String name, String city, String country, String IATA)
	{
		//TODO Cree un nuevo nodo (MapNode)
		
		//TODO Agregue el nodo a nodes
		
		keys.add(id);
	}

	/**
	 * A√±ade un arco entre dos nodos, cuya distancia se encuentra expresada en metros.
	 *
	 * @param from Nodo que establece el inicio del segmento.
	 * @param to Nodo final del segmento.
	 * @param distance Distancia geogr√°fica real entre los dos nodos solicitados.
	 */
	public void addEdge(int from, int to, int airline, double cost)
	{
		double distance = calculateDistance(from, to);
		//TODO Cree un nuevo arco (MapEdge)
		
		//TODO Agruegue el nuevo arco a edges, tenga en cuenta que puede ser la primera vez
		//que agrega un arco a los arcos que salen de "from"
	}


	/**
	 * Busca un aeropuerto de aceurdo con el codigo IATA
	 * @param IATA Codigo que se desea buscar
	 * @return El nodo si lo ecuentra, null de lo contrario
	 */
	public MapNode getNodeByIATA(String IATA){
		for (Integer key: keys){
			MapNode node = nodes.get(key);
			if (node.IATA.equals(IATA)){
				return node;
			}
		}
		return null;
	}

	/**
	 * Bas√°ndose en el identificador de un nodo, obtiene la informaci√≥n respectiva del nodo
	 * solicitado.
	 *
	 * @param id N√∫mero de identificaci√≥n √∫nico de un nodo en el mapa. id >= 0
	 * @return La informaci√≥n completa de descripci√≥n del nodo solicitado.
	 * @see MapNode
	 */
	public MapNode getMapNode(int id)
	{
		//TODO Devuelva el nodo por id
		return null;
	}

	/**
	 * Bas√°ndose en el identificador de un nodo, obtiene el conjunto de arcos que tienen como
	 * origen, el nodo solicitado.
	 *
	 * @param id N√∫mero de identificaci√≥n √∫nico de un nodo en el mapa. id >= 0
	 * @return El conjunto de arcos que tienen como origen, el nodo identificado con n√∫mero id.
	 * @see Set
	 * @see MapEdge
	 */
	public Set<MapEdge> getNodeEdges(int id)
	{
		//TODO Devuelva los arcos que salen del nodo por id
		return null;
	}

	/**
	 * Función para calcular la distancia en kilometros que hay entre dos aeropuertos
	 * @param from - Aeropuerto de origen
	 * @param to - Aeropuerto de destino
	 * @return La distancia en KM entre el aeropuerto de origen y el de destino.
	 */
	private double calculateDistance(int from, int to) {
		MapNode fromNode = getMapNode(from);
		MapNode toNode = getMapNode(to);

		double lat1=fromNode.lat;
		double lng1=fromNode.lng;
		double lat2=toNode.lat;
		double lng2=toNode.lng;

		int R = 6371; // Radius of the earth in km
		double dLat = deg2rad(lat2-lat1);  // deg2rad below
		double dLon = deg2rad(lng2-lng1); 
		double a = 
				Math.sin(dLat/2) * Math.sin(dLat/2) +
				Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
				Math.sin(dLon/2) * Math.sin(dLon/2)
				; 
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		double d = R * c; // Distance in km
		return d;
	}

	/**
	 * Función de ayuda para calcular la distancia entre dos nodos.
	 */
	private double deg2rad(double deg) {
		return deg * (Math.PI/180);
	}

	/**
	 * Función para determinar si el grafo está vacio.
	 * @return true - si está vacio
	 * @return false - si tiene algún nodo
	 */
	public boolean isEmpty(){
		return nodes.isEmpty();
	}

	@Override
	public Iterator<MapNode> iterator() {
		return new Iterator<MapNode>() {

			Integer pos = -1;
			Integer key;

			@Override
			public boolean hasNext() {
				return ((pos+1) < keys.size());
			}

			@Override
			public MapNode next() {
				if (hasNext()){
					pos++;
					key = keys.get(pos);
					return nodes.get(key);
				}
				return null;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
		};
	}
}
